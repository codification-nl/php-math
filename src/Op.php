<?php

namespace Codification\Math
{
	final class Op
	{
		public const ADD  = 'add';
		public const SUB  = 'sub';
		public const MUL  = 'mul';
		public const DIV  = 'div';
		public const COMP = 'comp';
		public const EQ   = 'eq';
		public const NE   = 'ne';
		public const GT   = 'gt';
		public const LT   = 'lt';
		public const GTE  = 'gte';
		public const LTE  = 'lte';
	}
}