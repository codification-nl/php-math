<?php

namespace Codification\Math
{
	/**
	 * @method \Codification\Math\Number|string add(\Codification\Math\Number|string|float|int $other)
	 * @method \Codification\Math\Number|string sub(\Codification\Math\Number|string|float|int $other)
	 * @method \Codification\Math\Number|string mul(\Codification\Math\Number|string|float|int $other)
	 * @method \Codification\Math\Number|string div(\Codification\Math\Number|string|float|int $other)
	 * @method bool eq(\Codification\Math\Number|string|float|int $other)
	 * @method bool ne(\Codification\Math\Number|string|float|int $other)
	 * @method bool gt(\Codification\Math\Number|string|float|int $other)
	 * @method bool lt(\Codification\Math\Number|string|float|int $other)
	 * @method bool gte(\Codification\Math\Number|string|float|int $other)
	 * @method bool lte(\Codification\Math\Number|string|float|int $other)
	 */
	final class Number implements \JsonSerializable
	{
		/** @var string */
		private $value;

		/** @var int */
		private $scale;

		/**
		 * @param int|float|string $value
		 * @param int|null         $scale = null
		 */
		public function __construct($value, int $scale = null)
		{
			$this->setValue($value);
			$this->setScale($scale);
		}

		/**
		 * @return string
		 */
		public function getValue() : string
		{
			return $this->value;
		}

		/**
		 * @param int|float|string $value
		 *
		 * @return $this|string
		 */
		public function setValue($value) : self
		{
			$this->value = Math::trim($value);

			return $this;
		}

		/**
		 * @return int
		 */
		public function getScale() : int
		{
			return $this->scale;
		}

		/**
		 * @param int|null $scale = null
		 *
		 * @return $this|string
		 */
		public function setScale(int $scale = null) : self
		{
			$this->scale = $scale ?? Math::$scale;

			return $this;
		}

		/**
		 * @param string $name
		 * @param mixed  $arguments
		 *
		 * @return \Codification\Math\Number|string|bool
		 */
		public function __call($name, $arguments)
		{
			[
				/** @var string $other */
				$other,
			] = $arguments;

			switch ($name)
			{
				case Op::EQ:
				case Op::NE:
				case Op::GT:
				case Op::LT:
				case Op::GTE:
				case Op::LTE:
					$op = Op::COMP;
					break;

				default:
					$op = $name;
			}

			$value = Math::bc($op, $this, $other, $this->scale);

			if ($op === Op::COMP)
			{
				return self::comp($name, $value);
			}

			return number($value, $this->scale);
		}

		/**
		 * @param string $name
		 * @param int    $value
		 *
		 * @return bool
		 */
		private static function comp(string $name, int $value) : bool
		{
			switch ($name)
			{
				case Op::EQ:
					return ($value === Comp::EQ);

				case Op::NE:
					return ($value !== Comp::EQ);

				case Op::GT:
					return ($value === Comp::GT);

				case Op::LT:
					return ($value === Comp::LT);

				case Op::GTE:
					return ($value === Comp::GT || $value === Comp::EQ);

				case Op::LTE:
					return ($value === Comp::LT || $value === Comp::EQ);

				default:
					throw new \UnexpectedValueException();
			}
		}

		/**
		 * @return int
		 */
		public function toInt() : int
		{
			return intval($this->value);
		}

		/**
		 * @return float
		 */
		public function toFloat() : float
		{
			return floatval($this->value);
		}

		/**
		 * @return \Codification\Math\Number|string
		 */
		public function copy() : \Codification\Math\Number
		{
			return clone $this;
		}

		/**
		 * @return \Codification\Math\Number|string
		 */
		public function clone() : \Codification\Math\Number
		{
			return clone $this;
		}

		/**
		 * @return string
		 */
		public function jsonSerialize() : string
		{
			return $this->getValue();
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->getValue();
		}

		/**
		 * @return \Codification\Math\Number|string
		 */
		public function __clone()
		{
			return number($this->value, $this->scale);
		}
	}
}