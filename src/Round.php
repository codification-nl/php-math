<?php

namespace Codification\Math
{
	final class Round
	{
		public const UP   = PHP_ROUND_HALF_UP;
		public const DOWN = PHP_ROUND_HALF_DOWN;
	}
}