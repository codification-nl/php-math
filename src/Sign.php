<?php

namespace Codification\Math
{
	final class Sign
	{
		public const POSITIVE = 1;
		public const NEGATIVE = -1;
		public const ZERO     = 0;
	}
}