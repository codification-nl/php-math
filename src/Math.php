<?php

namespace Codification\Math
{
	/**
	 * @method static \Codification\Math\Number|string add(\Codification\Math\Number|string|float|int $lhs, \Codification\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Math\Number|string sub(\Codification\Math\Number|string|float|int $lhs, \Codification\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Math\Number|string mul(\Codification\Math\Number|string|float|int $lhs, \Codification\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Math\Number|string div(\Codification\Math\Number|string|float|int $lhs, \Codification\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Math\Number|string pow(\Codification\Math\Number|string|float|int $base, string|int $exponent, int|null $scale = null)
	 */
	final class Math
	{
		/** @var int */
		public static $scale = 8;

		/**
		 * @param string $name
		 * @param mixed  $arguments
		 *
		 * @return \Codification\Math\Number|string
		 */
		public static function __callStatic($name, $arguments)
		{
			[
				/** @var \Codification\Math\Number|string|float|int $lhs */
				$lhs,
				/** @var \Codification\Math\Number|string|float|int $rhs */
				$rhs,
				/**  @var int|null $scale */
				$scale,
			] = $arguments + [null, null, null];

			if ($scale === null)
			{
				if ($lhs instanceof Number)
				{
					$scale = $lhs->getScale();
				}
				else
				{
					$scale = static::$scale;
				}
			}

			return number(Math::bc($name, $lhs, $rhs, $scale), $scale);
		}

		public static function sign($value) : int
		{
			$result = static::getValue($value);

			if ($result === '0')
			{
				return Sign::ZERO;
			}

			if (static::isNegative($result))
			{
				return Sign::NEGATIVE;
			}

			return Sign::POSITIVE;
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 * @param int|string                                 $precision = 0
		 * @param int|null                                   $mode      = null
		 *
		 * @return \Codification\Math\Number|string
		 */
		public static function round($value, int $precision = 0, int $mode = null) : \Codification\Math\Number
		{
			/** @var int|null $scale */
			$scale  = null;
			$result = static::getValue($value, $scale);

			switch ($mode)
			{
				case Round::UP:
				case Round::DOWN:
					{
						$op = ($mode === Round::UP) ? 'ceil' : 'floor';

						$rhs    = bcpow('10', $precision, 0);
						$lhs    = static::{$op}(bcmul($result, $rhs, static::getDecimals($result)));
						$result = bcdiv($lhs, $rhs, $precision);

						return number($result, $scale);
					}

				default:
					{
						$op = static::isNegative($result) ? 'sub' : 'add';

						$rhs    = '0.' . str_repeat('0', $precision) . '5';
						$result = static::bc($op, $result, $rhs, $precision);

						return number($result, $scale);
					}
			}
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 * @param int|null                                   $scale = null
		 *
		 * @return \Codification\Math\Number|string
		 */
		public static function floor($value, int $scale = null) : \Codification\Math\Number
		{
			$result = static::getValue($value, $scale);

			if (static::hasDecimals($result))
			{
				$rhs    = static::isNegative($result) ? '1' : '0';
				$result = bcsub($result, $rhs, 0);
			}

			return number($result, $scale);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 * @param int|null                                   $scale = null
		 *
		 * @return \Codification\Math\Number|string
		 */
		public static function ceil($value, int $scale = null) : \Codification\Math\Number
		{
			$result = static::getValue($value, $scale);

			if (static::hasDecimals($result))
			{
				$rhs    = static::isNegative($result) ? '0' : '1';
				$result = bcadd($result, $rhs, 0);
			}

			return number($result, $scale);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 * @param int|null                                   $scale = null
		 *
		 * @return \Codification\Math\Number|string
		 */
		public static function abs($value, int $scale = null) : \Codification\Math\Number
		{
			$result = static::getValue($value, $scale);

			if (static::isNegative($result))
			{
				$result = substr($result, 1);
			}

			return number($result, $scale);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 *
		 * @return bool
		 */
		private static function hasDecimals($value) : bool
		{
			return (strpos($value, '.') !== false);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 *
		 * @return bool
		 */
		private static function isNegative($value) : bool
		{
			return (strncmp('-', $value, 1) === 0);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 *
		 * @return int
		 */
		private static function getDecimals($value) : int
		{
			if (!static::hasDecimals($value))
			{
				return 0;
			}

			[, $decimals] = explode('.', $value, 2);

			return strlen($decimals);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 * @param int|null                                  &$scale = -1
		 *
		 * @return string
		 */
		private static function getValue($value, &$scale = -1) : string
		{
			if ($value instanceof \Codification\Math\Number)
			{
				if ($scale !== -1 && $scale === null)
				{
					$scale = $value->getScale();
				}

				return $value->getValue();
			}

			return static::trim($value);
		}

		/**
		 * @param string     $op
		 * @param string|int ...$params
		 *
		 * @return string|int - Returns 'trimmed' result.
		 */
		public static function bc(string $op, ...$params)
		{
			$value = call_user_func_array("bc{$op}", $params);

			if ($op === Op::COMP)
			{
				return $value;
			}

			return static::trim($value);
		}

		/**
		 * @param \Codification\Math\Number|string|int|float $value
		 *
		 * @return string
		 */
		public static function trim($value) : string
		{
			if ($value instanceof \Codification\Math\Number)
			{
				return $value->getValue();
			}

			$value = strval($value);
			$value = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$value = str_replace('+', '', $value);

			if (static::hasDecimals($value))
			{
				$value = rtrim($value, '0');
			}

			$value = rtrim($value, '.');

			if (!$value || $value === '-0')
			{
				return '0';
			}

			return $value;
		}
	}
}