<?php

namespace Codification\Math
{
	final class Comp
	{
		public const EQ = 0;
		public const GT = 1;
		public const LT = -1;
	}
}