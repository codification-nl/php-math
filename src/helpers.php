<?php

if (!function_exists('number'))
{
	/**
	 * @param int|float|string $value
	 * @param int|null         $scale = null
	 *
	 * @return \Codification\Math\Number
	 */
	function number($value, int $scale = null) : \Codification\Math\Number
	{
		return new \Codification\Math\Number($value, $scale);
	}
}