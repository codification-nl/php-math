<?php

namespace Codification\Math\Tests
{
	use Codification\Math\Op;
	use PHPUnit\Framework\TestCase;

	class NumberTest extends TestCase
	{
		/** @test */
		public function it_can_add_value_type()
		{
			$object = number(1, 8)->add(0.6);
			$this->assertEquals('1.6', $object->getValue());

			$object = number(1, 8);
			$object->add(0.6);
			$this->assertNotEquals('1.6', $object->getValue());
		}

		/** @test */
		public function it_can_add_number()
		{
			$object = number(1, 8)->add(number(0.6));
			$this->assertEquals('1.6', $object->getValue());

			$object = number(1, 8);
			$object->add(number(0.6));
			$this->assertNotEquals('1.6', $object->getValue());
		}

		/** @test */
		public function it_can_have_scale()
		{
			$object = number(1, 2)->div(0.6);
			$this->assertEquals('1.66', $object->getValue());

			$object = number(1, 2);
			$object->div(0.6);
			$this->assertNotEquals('1.66', $object->getValue());
		}

		/** @test */
		public function it_can_cast_to_string()
		{
			$this->assertEquals((string)1, (string)number(1));
		}

		/** @test */
		public function it_can_encode_to_json()
		{
			$this->assertEquals(json_encode('1'), json_encode(number(1)));
		}

		/** @test */
		public function it_can_compare()
		{
			$scale = 8;

			$tests = [
				['1.0000000000', '0.6000000000'],
				['1.0000000000', '1.0000000000'],
				['1.6200000000', '1.6800000000'],
				['1.6666666699', '1.6666666611'],
			];

			$operators = [
				Op::EQ  => [false, true, false, true],
				Op::NE  => [true, false, true, false],
				Op::GT  => [true, false, false, false],
				Op::LT  => [false, false, true, false],
				Op::GTE => [true, true, false, true],
				Op::LTE => [false, true, true, true],
			];

			foreach ($operators as $operator => $results)
			{
				foreach ($tests as $index => [$a, $b])
				{
					$expected = $results[$index];

					$object = number($a, $scale);
					$object = $object->{$operator}($b);

					$result  = $expected ? 'true' : 'false';
					$message = "{$operator}[{$index}] => (number('{$a}', {$scale})->{$operator}('{$b}') === {$result})";

					$this->assertEquals($expected, $object, $message);
				}
			}
		}
	}
}