<?php

namespace Codification\Math\Tests
{
	use Codification\Math\Math;
	use Codification\Math\Op;
	use Codification\Math\Round;
	use PHPUnit\Framework\TestCase;

	class MathTest extends TestCase
	{
		/** @test */
		public function it_can_add_value_type()
		{
			$object = Math::add(number(1, 8), 0.6);
			$this->assertEquals('1.6', $object->getValue());
		}

		/** @test */
		public function it_can_add_number()
		{
			$object = Math::add(number(1, 8), number(0.6));
			$this->assertEquals('1.6', $object->getValue());
		}

		/** @test */
		public function it_can_have_scale()
		{
			$object = Math::div(number(1, 2), 0.6);
			$this->assertEquals('1.66', $object->getValue());
		}

		/** @test */
		public function it_can_round()
		{
			$object = Math::round(number(1.4, 8));
			$this->assertEquals('1', $object->getValue());
			$object = Math::round(number(-1.4, 8));
			$this->assertEquals('-1', $object->getValue());

			$object = Math::round(number(1.66, 8));
			$this->assertEquals('2', $object->getValue());
			$object = Math::round(number(-1.66, 8));
			$this->assertEquals('-2', $object->getValue());
		}

		/** @test */
		public function it_can_round_up()
		{
			$object = Math::round(number(1.66, 8), 0, Round::UP);
			$this->assertEquals('2', $object->getValue());
			$object = Math::round(number(-1.66, 8), 0, Round::UP);
			$this->assertEquals('-1', $object->getValue());

			$object = Math::round(number(1.66, 8), 1, Round::UP);
			$this->assertEquals('1.7', $object->getValue());
			$object = Math::round(number(-1.66, 8), 1, Round::UP);
			$this->assertEquals('-1.6', $object->getValue());

			$object = Math::round(number(1.66, 8), 2, Round::UP);
			$this->assertEquals('1.66', $object->getValue());
			$object = Math::round(number(-1.66, 8), 2, Round::UP);
			$this->assertEquals('-1.66', $object->getValue());
		}

		/** @test */
		public function it_can_round_down()
		{
			$object = Math::round(number(1.66, 8), 0, Round::DOWN);
			$this->assertEquals('1', $object->getValue());
			$object = Math::round(number(-1.66, 8), 0, Round::DOWN);
			$this->assertEquals('-2', $object->getValue());

			$object = Math::round(number(1.66, 8), 1, Round::DOWN);
			$this->assertEquals('1.6', $object->getValue());
			$object = Math::round(number(-1.66, 8), 1, Round::DOWN);
			$this->assertEquals('-1.7', $object->getValue());

			$object = Math::round(number(1.66, 8), 2, Round::DOWN);
			$this->assertEquals('1.66', $object->getValue());
			$object = Math::round(number(-1.66, 8), 2, Round::DOWN);
			$this->assertEquals('-1.66', $object->getValue());
		}

		/** @test */
		public function it_can_floor()
		{
			$object = Math::floor(number(1.4, 8));
			$this->assertEquals('1', $object->getValue());
			$object = Math::floor(number(-1.4, 8));
			$this->assertEquals('-2', $object->getValue());

			$object = Math::floor(number(1.66, 8));
			$this->assertEquals('1', $object->getValue());
			$object = Math::floor(number(-1.66, 8));
			$this->assertEquals('-2', $object->getValue());
		}

		/** @test */
		public function it_can_ceil()
		{
			$object = Math::ceil(number(1.4, 8));
			$this->assertEquals('2', $object->getValue());
			$object = Math::ceil(number(-1.4, 8));
			$this->assertEquals('-1', $object->getValue());

			$object = Math::ceil(number(1.66, 8));
			$this->assertEquals('2', $object->getValue());
			$object = Math::ceil(number(-1.66, 8));
			$this->assertEquals('-1', $object->getValue());
		}
	}
}